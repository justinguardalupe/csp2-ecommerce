const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

// create/add product *Admin Only
router.post('/', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	productController.createProduct(req.body, userData)
	.then(result => res.send(result));
});

// retrieve all products
router.get('/', (req, res) => {
	productController.getAllProducts()
	.then(result => res.send(result));
});

// retrieve a specific product
router.get('/:productId', (req, res) => {
	productController.getProduct(req.params)
	.then(result => res.send(result));
});

// update a product * ADMIN Only
router.put("/:productId", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization);

	productController.updateProduct(user, req.params, req.body)
	.then(resultFromController => res.send(resultFromController));
})

// archive/delete specific product *ADMIN Only
router.put('/:productId/archive', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	
	productController.archiveProduct(userData, req.params)
	.then(result => res.send(result));
});


module.exports = router;
