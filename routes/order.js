const express = require("express");
const router = express.Router();
const orderController = require("../controllers/order");
const auth = require("../auth");

// create order
router.post("/users/checkout", auth.verify, (req, res) => {
	
	const user = auth.decode(req.headers.authorization);

	orderController.createOrder(user, req.body)
	.then(resultFromController => res.send(resultFromController));
})

// retrieve all orders
router.get("/users/orders", (req, res) => {
	productController.retrieveAllProducts()
	.then(resultFromController => res.send(resultFromController));
})

// retrieve authenticated user's orders
router.get("/users/orders", (req, res) => {
	productController.retrieveAllProducts()
	.then(resultFromController => res.send(resultFromController));
})