const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/user");
// const orderRoutes = require("./routes/order");
const productRoutes = require("./routes/product");

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

//to connect to mongoDB Atlas
mongoose.connect("mongodb+srv://dbjustinguardalupe:4bl1kYu9JtcqB106@wdc028-course-booking.gqwer.mongodb.net/csp2-ecommerce?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}	
);

//to check if we are connected to MongoDB
mongoose.connection.once('open',() => console.log('Now connected to MongoDB Atlas'));

app.use("/users", userRoutes);
app.use("/products", productRoutes);


//environment variable port or port 4000
app.listen(process.env.PORT || 4000, () => {
	console.log(`API on Port ${ process.env.PORT || 4000 } is now online`)
});
