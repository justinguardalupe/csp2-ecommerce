const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	totalAmount : {
		type : Number,
		default : 0
	},
	purchasedOn : {
		type : Date,
		default : new Date()
	},
	otherDetails : [{
		userId : {
			type : String,
			required : [true, "User ID is required"]
		},
		productIDs : {
			type : Array
		}
	}]
});

module.exports = mongoose.model("Order", orderSchema);
