const Order = require("../models/Order");

// create order *USER only
module.exports.createOrder = async (user, reqBody) => {
	if (user.isUser) {
		let newOrder = new Order({
			totalAmount : reqBody.totalAmount,
			purchasedOn : reqBody.purchasedOn,
			otherDetails : reqBody.otherDetails
		})

		return newOrder.save().then((order, error) => {
			if (error) {
				return false;
			}
			else {
				return ('Order creation was successful');
			}
		})
	}
	else {
		return "You don't have access";
	}
}

// retrieve all orders
module.exports.retrieveAllOrders = () => {
	return Order.find().then(result => {
		return result;
	})
}

// retrieve user orders
module.exports.retrieveOrder = (reqParams) => {
	return Order.findById(reqParams.productId).then(result => {
		return result;
	})
}