const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

// Route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body)
	.then(resultFromController => res.send(`${resultFromController}`));
});


// Route for retrieving all users (ADMIN ONLY)
router.get('/all', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getAllUsers(userData).then(result => res.send(result));
})

// Route for authenticating a user
router.post("/login", (req, res) => {
	userController.loginUser(req.body)
	.then(resultFromController => res.send(resultFromController));
});

// Route for setting first admin
router.put("/setFirstAdmin/:id", (req, res) => {
	userController.setFirstAdmin(req.params.id)
	.then(resultFromController => res.send(resultFromController));
})

//Route for setting a user's admin access (ADMIN ONLY)
router.put("/:id/setAsAdmin", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.setAsAdmin(userData, req.params.id, req.body)
	.then(resultFromController => res.send(resultFromController));
});

module.exports = router;